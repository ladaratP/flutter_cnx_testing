import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyCustomWidget extends StatelessWidget {
  final String title;
  final String message;

  const MyCustomWidget({Key key, this.title, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Center(
          child: Text(message),
        ),
      ),
    );
  }
}
