import 'package:flutter/material.dart';
import 'package:flutter_cnx_testing/first_page.dart';
import 'package:flutter_cnx_testing/second_page.dart';

import 'calculate.dart';
import 'custom_widget.dart';

void main() {
  runApp(NavigateApp());
}

class NavigateApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => FirstPage(),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/second': (context) => SecondPage(),
      },
    );
  }
}

class MyNavigatePage extends StatefulWidget {
  MyNavigatePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyNavigatePage createState() => _MyNavigatePage();
}

class _MyNavigatePage extends State<MyNavigatePage> {
  int _counter = 0;
  Calculate _calculate = Calculate();

  void _incrementCounter() {
    setState(() {
      _counter = _calculate.plus(_counter, 1);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            MyWidget(
              title: 'Test widget title',
              message: 'Test widget message',
            ),
          ],
        ),
      ),
    );
  }
}
