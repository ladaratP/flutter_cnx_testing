import 'package:flutter_cnx_testing/calculate.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  Calculate _calculate;
  setUpAll(() async {
    _calculate = Calculate();
  });

  tearDownAll(() async {});

  test('Additional calculate', () {
    int count = 1;
    int summary = 40;
    int expectSummary = 41;
    var total = _calculate.plus(summary, count);
    expect(total, expectSummary);
  });
}
